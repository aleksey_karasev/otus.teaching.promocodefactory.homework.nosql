﻿
namespace Otus.Teaching.Pcf.Administration.Core.Domain
{
    public class BaseMongoEntity
    {
        //для MongoDB нужен тип Object, чтобы не создавался дубликат айди.
        public object Id { get; set; }
    }
}
