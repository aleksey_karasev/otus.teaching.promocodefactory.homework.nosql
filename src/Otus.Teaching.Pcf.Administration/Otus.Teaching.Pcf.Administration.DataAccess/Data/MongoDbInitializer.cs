﻿using MongoDB.Driver;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly MongoDataContext _dataContext;

        public MongoDbInitializer(MongoDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            var employeeCollection = _dataContext.Employees;
            var roleCollection = _dataContext.Roles;

            employeeCollection.InsertMany(FakeDataFactory.Employees);
            roleCollection.InsertMany(FakeDataFactory.Roles);
        }
    }
}