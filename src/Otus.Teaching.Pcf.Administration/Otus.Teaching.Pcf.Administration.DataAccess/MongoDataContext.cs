﻿

using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;

namespace Otus.Teaching.Pcf.Administration.DataAccess
{
    public class MongoDataContext
    {
        public readonly IMongoDatabase _database;

        public MongoDataContext(string connectionString, string databaseName)
        {
            var client = new MongoClient(connectionString);
            _database = client.GetDatabase(databaseName);
        }

        public IMongoCollection<Employee> Employees => _database.GetCollection<Employee>("Employees");

        public IMongoCollection<Role> Roles => _database.GetCollection<Role>("Roles");
    }
}
