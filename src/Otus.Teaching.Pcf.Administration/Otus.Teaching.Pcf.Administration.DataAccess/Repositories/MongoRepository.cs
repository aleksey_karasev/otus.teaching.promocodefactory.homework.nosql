﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T>
        : IRepositoryMongo<T>
        where T: BaseMongoEntity
    {
        private IMongoCollection<T> _collection;

        private readonly MongoDataContext _mongoDataContext;

        public MongoRepository(MongoDataContext mongoDataContext)
        {
            _mongoDataContext = mongoDataContext;

            _collection = _mongoDataContext._database.GetCollection<T>(typeof(T).Name);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _collection.Find(_ => true).ToListAsync();

            return entities;
        }

        public async Task<T> GetByIdAsync(Object id)
        {
            var entity = await _collection.Find(f => f.Id == id).FirstOrDefaultAsync();

            return entity;
        }

        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<Object> ids)
        {
            var filter = Builders<T>.Filter.In("Id", ids);
            return await _collection.Find(filter).ToListAsync();
        }

        public async Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return await _collection.Find(predicate).ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _collection.InsertOneAsync(entity);
        }

        public async Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq("_id", entity.Id);
            await _collection.ReplaceOneAsync(filter, entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq("_id", entity.Id);
            await _collection.DeleteOneAsync(filter);
        }
    }
}